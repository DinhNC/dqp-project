-- ETL Test 3: 204 records where UPAI 15 <> substr(UPAI 18,1,15)
SELECT 
  case when a.id is null then 1 
    else 0 
  end as MSNG_A,
  case when b.id is null then 1 
    else 0 
  end as MSNG_B,
  case when createddate > date_From_parts(2019,8,21) then 1 
    else 0 
  end as CREATED_LATER,
  case when substr(b.ultimate_parent_account_id,1,15)=ULTIMATE_PARENT_ACCOUNT_ID__C then 1 
    else 0 
  end as MATCH_UPAI_15,
  count(*)
from
(
  select id,createddate,ULTIMATE_PARENT_ACCOUNT_ID__C
  from RAW.salesforce_stitch.account
  where isdeleted = FALSE and id is not null
) a
left join
(
  select account_id as id,ultimate_parent_account_id
  from ANALYTICS.pwc_staging.sfdc_account
) b
  on a.id=b.id
group by 1,2,3,4
