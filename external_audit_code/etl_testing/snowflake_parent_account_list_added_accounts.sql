/*
 * Note: Due to sensitive information, this query has been modified to remove the ids 
 */

-- ETL Test 3c: Retrieve all UPAI_15s for the added 35 Account IDs
-- ETL Test 3c: Retrieve all UPAI_15s for the added 35 Account IDs
select id,ULTIMATE_PARENT_ACCOUNT_ID__C
from RAW.salesforce_stitch.account
where id in
(
    'INSERT LIST OF ID NUMBERS HERE'
) 
