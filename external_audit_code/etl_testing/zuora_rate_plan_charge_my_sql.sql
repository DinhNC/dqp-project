--MYSQL
CREATE INDEX IDX_zrpc ON rd.zuora_rateplancharge (rate_plan_charge_id);
CREATE INDEX IDX_snfrpc ON rd.snf_rateplancharge (id);

DROP TABLE IF EXISTS TEMP_ETLTest_5;
CREATE TABLE TEMP_ETLTest_5 AS
SELECT A.*, B.*
FROM rd.zuora_rateplancharge A FORCE INDEX(IDX_zrpc)
LEFT JOIN rd.snf_rateplancharge B FORCE INDEX(IDX_snfrpc)
  ON A.RATE_PLAN_charge_ID =B.ID;

DROP TABLE IF EXISTS RPCHARGEID_NOTINSNF;
CREATE TABLE RPCHARGEID_NOTINSNF AS
SELECT 
  RATE_PLAN_CHARGE_ID,
  RATE_PLAN_CHARGE_EFFECTIVE_START_DATE,
  RATE_PLAN_CHARGE_EFFECTIVE_END_DATE,
  RATE_PLAN_CHARGE_MRR,
  RATE_PLAN_CHARGE_TCV,
  RATE_PLAN_CHARGE_UNIT_OF_MEASURE,
  DELETED
FROM TEMP_ETLTest_5
WHERE ID IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTest_6;
CREATE TABLE TEMP_ETLTest_6 AS
SELECT A.*, B.*
FROM rd.snf_rateplancharge B FORCE INDEX(IDX_SNFRPC)
LEFT JOIN rd.zuora_rateplancharge A FORCE INDEX(IDX_ZRPC)
  ON B.ID = A.Rate_plan_charge_id
WHERE DELETED = 'FALSE';

DROP TABLE IF EXISTS RPCHARGEID_NOTINzuora;
CREATE TABLE RPCHARGEID_NOTINzuora AS
SELECT 
  ID,
  EFFECTIVESTARTDATE,
  EFFECTIVEENDDATE,
  MRR,
  TCV,
  UOM,
  DELETED
FROM TEMP_ETLTest_6
WHERE rate_plan_CHARGE_id IS NULL;


DROP TABLE IF EXISTS TEMP_ETLTEST_65;
CREATE TABLE TEMP_ETLTEST_65 AS
SELECT 
  A.RATE_PLAN_CHARGE_ID,
  A.RATE_PLAN_CHARGE_EFFECTIVE_START_DATE,
  A.RATE_PLAN_CHARGE_EFFECTIVE_END_DATE,
  A.RATE_PLAN_CHARGE_MRR,
  A.RATE_PLAN_CHARGE_TCV,
  A.RATE_PLAN_CHARGE_UNIT_OF_MEASURE,
  B.ID,
  B.MRR,
  B.TCV,
  B.UOM,
  A.RATE_PLAN_CHARGE_MRR - B.MRR AS MRR_DIFFERENCE,
  A.RATE_PLAN_CHARGE_TCV - B.TCV AS TCV_DIFFERENCE,
  CASE WHEN A.RATE_PLAN_CHARGE_UNIT_OF_MEASURE != B.UOM THEN 'X'
    ELSE '' 
  END AS UOM_DIFFERENCE
FROM rd.snf_rateplancharge B FORCE INDEX(IDX_SNFRPC)
INNER JOIN rd.zuora_rateplancharge A FORCE INDEX(IDX_ZRPC)
  ON B.ID = A.Rate_plan_charge_id
WHERE B.DELETED = 'FALSE';

DROP TABLE IF EXISTS RPCHARGEID_DIFF;
CREATE TABLE RRPCHARGEID_DIFF AS
SELECT * FROM TEMP_ETLTEST_65
WHERE abs(MRR_DIFFERENCE) >= 0.01
  OR  abs(TCV_DIFFERENCE) >= 0.01
  OR UOM_DIFFERENCE = 'X';
-- not comparing effective starting and ending date because known difference due to timezone impact on data extraction
