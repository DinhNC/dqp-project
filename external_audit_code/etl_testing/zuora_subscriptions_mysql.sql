CREATE INDEX IDX_ZSubs ON rd.zuora_subscription (subscription_name, subscription_id);
CREATE INDEX IDX_SFSubs ON rd.snf_subscription (`name`, id);

DROP TABLE IF EXISTS TEMP_ETLTest_1;
CREATE TABLE TEMP_ETLTest_1 AS SELECT
*
FROM rd.zuora_subscription A FORCE INDEX(IDX_ZSUBS)
LEFT JOIN rd.snf_subscription B FORCE INDEX(IDX_SFSUBS)
ON A.SUBSCRIPTION_NAME = B.NAME
AND A.SUBSCRIPTION_ID = B.ID;
-- Zuora table does not have Account ID or Deleted included.

DROP TABLE IF EXISTS SUBS_NOTINSNF;
CREATE TABLE SUBS_NOTINSNF AS
SELECT subscription_name,
subscription_id,
subscription_status,
subscription_contract_effective_date,
subscription_created_date,
deleted
FROM TEMP_ETLTest_1
WHERE `NAME` IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTest_2;
CREATE TABLE TEMP_ETLTest_2 AS SELECT
*
FROM rd.snf_subscription B FORCE INDEX(IDX_SFSUBS)
LEFT JOIN rd.zuora_subscription A FORCE INDEX(IDX_ZSUBS)
ON A.SUBSCRIPTION_NAME = B.NAME
AND A.SUBSCRIPTION_ID = B.ID
WHERE B.DELETED = 'FALSE';

DROP TABLE IF EXISTS SUBS_NOTINZUORA;
CREATE TABLE SUBS_NOTINZUORA AS
SELECT name,
ID,
status,
contracteffectivedate,
createddate,
deleted
FROM TEMP_ETLTest_2
WHERE SUBSCRIPTION_NAME IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTEST_25;
CREATE TABLE TEMP_ETLTEST_25 AS
SELECT A.subscription_name,
A.subscription_id,
A.subscription_status,
A.subscription_contract_effective_date,
A.subscription_created_date,
B.name,
B.ID,
B.status,
B.contracteffectivedate,
B.createddate,
B.deleted,
CASE WHEN A.subscription_status != B.STATUS THEN 'X'
ELSE '' END AS STATUS_DIFFERENCE,
CASE WHEN DATE_FORMAT(STR_TO_DATE(A.subscription_contract_effective_date, '%m/%d/%Y %H:%i'), '%Y-%m-%d') != SUBSTRING(B.contracteffectivedate,1,10) THEN 'X'
ELSE '' END AS CONTRACT_EFFECTIVEDATE_DIFFERENCE
FROM rd.snf_subscription B FORCE INDEX(IDX_SFSUBS)
LEFT JOIN rd.zuora_subscription A FORCE INDEX(IDX_ZSUBS)
ON A.SUBSCRIPTION_NAME = B.NAME
AND A.SUBSCRIPTION_ID = B.ID
WHERE B.DELETED = 'FALSE';

DROP TABLE IF EXISTS SUBS_DIFFERENCE;
CREATE TABLE SUBS_DIFFERENCE AS
SELECT *
FROM TEMP_ETLTEST_25
WHERE STATUS_DIFFERENCE = 'X'
OR CONTRACT_EFFECTIVEDATE_DIFFERENCE = 'X';
-- Did not compare Name because difference are due to special characters
