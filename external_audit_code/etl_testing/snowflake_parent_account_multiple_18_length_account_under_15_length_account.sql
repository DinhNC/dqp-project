-- ETL Test 3e: Test whether there are any cases where there are multiple unique UPAI_18s under one UPAI_15.  unique_count = 1 in all cases
select row_count,unique_count,count(*)
from
(
  select substr(ultimate_parent_account_id,1,15) as upas,count(ultimate_parent_account_id) as row_count,
      count(distinct ultimate_parent_account_id) as unique_count
  from  ANALYTICS.pwc_staging.sfdc_account b
  group by 1
) bb
group by 1,2
