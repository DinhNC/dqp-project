--MYSQL
CREATE INDEX IDX_zinv ON rd.zuora_invoice (invoice_id);
CREATE INDEX IDX_snf_inv ON rd.snf_invoice (id);

DROP TABLE IF EXISTS TEMP_ETLTest_7;
CREATE TABLE TEMP_ETLTest_7 AS
SELECT A.*, B.*
FROM rd.zuora_invoice A FORCE INDEX(IDX_zinv)
LEFT JOIN rd.snf_invoice B FORCE INDEX(IDX_snf_inv)
  ON A.invoice_id =B.ID;

DROP TABLE IF EXISTS invoice_NOTINSNF;
CREATE TABLE invoice_NOTINSNF AS
SELECT
  invoice_id,
  invoice_created_date,
  invoice_invoice_number,
  invoice_status,
  deleted
FROM TEMP_ETLTest_7
WHERE ID IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTest_8;
CREATE TABLE TEMP_ETLTest_8 AS
SELECT A.*, B.*
FROM rd.snf_invoice A FORCE INDEX(IDX_snf_inv)
LEFT JOIN rd.zuora_invoice B FORCE INDEX(IDX_zinv)
  ON A.ID = B.invoice_id
WHERE DELETED = 'FALSE';

DROP TABLE IF EXISTS invoice_notinzuora;
CREATE TABLE invoice_notinzuora AS
SELECT 
  ID,
  createddate,
  invoicenumber,
  status,
  deleted
FROM TEMP_ETLTest_8
WHERE invoice_id IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTest_85;
CREATE TABLE TEMP_ETLTest_85 AS
SELECT 
  B.invoice_id,
  B.invoice_created_date,
  B.invoice_invoice_number,
  B.invoice_status,
  A.ID,
  A.CREATEDDATE,
  A.INVOICENUMBER,
  A.STATUS,
  A.DELETED,
  CASE WHEN A.STATUS != B.INVOICE_STATUS THEN 'X'
    ELSE '' 
  END AS STATUS_DIFFERENCE
FROM rd.snf_invoice A FORCE INDEX(IDX_snf_inv)
INNER JOIN rd.zuora_invoice B FORCE INDEX(IDX_zinv)
  ON A.ID = B.invoice_id
WHERE DELETED = 'FALSE';

DROP TABLE IF EXISTS INVOICE_DIFF;
CREATE TABLE INVOICE_DIFF AS
SELECT *
FROM TEMP_ETLTEST_85
WHERE STATUS_DIFFERENCE = 'X';
-- check is if status = "posted"
