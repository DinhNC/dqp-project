--MYSQL
DROP TABLE IF EXISTS TEMP_ETLTest_9;
CREATE TABLE TEMP_ETLTest_9 AS
SELECT A.*, B.*
FROM rd.zuora_invoiceitem A FORCE INDEX(IDX_zinvitem)
LEFT JOIN rd.snf_invoiceitem B FORCE INDEX(IDX_snf_invitem)
  ON A.invoice_item_id =B.ID;

DROP TABLE IF EXISTS invoiceitem_NOTINSNF;
CREATE TABLE invoiceitem_NOTINSNF AS
SELECT
  invoice_item_id,
  invoice_item_charge_name,
  invoice_item_charge_amount,
  invoice_item_created_date,
  invoice_item_service_start_date
FROM TEMP_ETLTest_9
WHERE ID IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTest_10;
CREATE TABLE TEMP_ETLTest_10 AS
SELECT A.*, B.*
FROM rd.snf_invoiceitem A FORCE INDEX(IDX_snf_invitem)
LEFT JOIN rd.zuora_invoiceitem B FORCE INDEX(IDX_zinvitem)
  ON A.ID = B.invoice_item_id
WHERE DELETED = 'FALSE';

DROP TABLE IF EXISTS invoiceitem_notinzuora;
CREATE TABLE invoiceitem_notinzuora AS
SELECT
  id,
  chargename,
  chargeamount,
  createddate,
  servicestartdate,
  deleted
FROM TEMP_ETLTest_10
WHERE invoice_item_id IS NULL;

DROP TABLE IF EXISTS TEMP_ETLTEST_105;
CREATE TABLE TEMP_ETLTEST_105 AS
SELECT A.invoice_item_id,
  A.invoice_item_charge_name,
  A.invoice_item_charge_amount,
  A.invoice_item_created_date,
  A.invoice_item_service_start_date,
  B.id,
  B.chargename,
  B.chargeamount,
  B.createddate,
  B.servicestartdate,
  B.deleted,
  A.invoice_item_charge_amount - B.chargeamount AS chargeamount_DIFFERENCE,
  CASE WHEN A.INVOICE_ITEM_CHARGE_NAME != B.CHARGENAME THEN 'X'
    ELSE '' 
  END AS NAME_DIFFERENCE,
  CASE WHEN substring(B.servicestartdate,1,10) != date_format(str_to_date(invoice_item_service_start_date, '%m/%d/%Y'),'%Y-%m-%d') THEN 'X'
    ELSE '' 
  END AS SERVICESTART_DIFFERENCE
FROM rd.snf_invoiceitem B FORCE INDEX(IDX_snf_invitem)
INNER JOIN rd.zuora_invoiceitem A FORCE INDEX(IDX_zinvitem)
  ON B.ID = A.invoice_item_id
WHERE DELETED = 'FALSE';


DROP TABLE IF EXISTS invoiceITEM_DIFF;
CREATE TABLE invoiceITEM_DIFF AS
SELECT *
FROM TEMP_ETLTEST_105
WHERE abs(chargeamount_difference) >= 0.01
  OR NAME_DIFFERENCE ='X'
  OR SERVICESTART_DIFFERENCE ='X';
