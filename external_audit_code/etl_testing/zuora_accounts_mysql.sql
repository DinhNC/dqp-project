CREATE INDEX IDX_zuoraaccountID ON rd.zuora_account (ACCOUNT_ID);					
CREATE INDEX IDX_snfaccountID ON rd.snf_account (ID);																															
																																		
DROP TABLE IF EXISTS TEMP_ETLTest_13;						
CREATE TABLE TEMP_ETLTest_13 AS				
SELECT A.*, B.*									
FROM rd.zuora_account A FORCE INDEX(IDX_zuoraaccountID)		
LEFT JOIN rd.snf_account B FORCE INDEX(IDX_snfaccountID)			
  ON A.Account_ID = B.ID;																																	
																																		
DROP TABLE IF EXISTS account_NOTINSNF;			
CREATE TABLE account_NOTINSNF AS					
SELECT Account_CRM_Account_ID, Account_Created_Date, account_id, account_name, DELETED, ID									
FROM TEMP_ETLTest_13														
WHERE ID IS NULL;																																	
																																		
DROP TABLE IF EXISTS TEMP_ETLTest_14;			
CREATE TABLE TEMP_ETLTest_14 AS					
SELECT A.*, B.*										
FROM rd.snf_account A FORCE INDEX(IDX_snfaccountID)		
LEFT JOIN rd.zuora_account B FORCE INDEX(IDX_zuoraaccountID)				
  ON A.ID = B.Account_ID									
WHERE DELETED = 'FALSE';																																	
																																		
DROP TABLE IF EXISTS account_notinzuora;				
CREATE TABLE account_notinzuora AS			
SELECT crmid, createddate, ID, name, DELETED, account_id			
FROM TEMP_ETLTest_14										
WHERE Account_ID IS NULL;																																	
																																		
DROP TABLE IF EXISTS TEMP_ETLTest_145;					
CREATE TABLE TEMP_ETLTest_145 AS						
SELECT B.Account_CRM_Account_ID,						
  B.Account_Created_Date,									
  B.ACCOUNT_ID,											
  b.account_name,										
  A.CRMID,																																	
  A.createddate,									
  A.id,																																	
  A.NAME,																																	
  A.Deleted,																																	
  CASE WHEN A.CRMID != B.ACCOUNT_CRM_ACCOUNT_ID THEN 'X'			
    ELSE '' 
  END AS DIFFERENCE_CRM_ID			
FROM rd.snf_account A FORCE INDEX(IDX_snfaccountID)				
INNER JOIN rd.zuora_account B FORCE INDEX(IDX_zuoraaccountID)			
  ON A.ID = B.Account_ID									
WHERE DELETED = 'FALSE';																																	
																																		
DROP TABLE IF EXISTS ACCOUNT_DIFFERENCE;			
CREATE TABLE ACCOUNT_DIFFERENCE AS				
SELECT * 
FROM temp_etltest_145					
WHERE DIFFERENCE_CRM_ID = 'X';																																	
-- Did not compare Name because difference are due to special characters																																	
