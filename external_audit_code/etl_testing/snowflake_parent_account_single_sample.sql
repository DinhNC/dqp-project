/*
 * Note: This query has been modified to remove sensitive information. 
 */ 

-- ETL Test 3d: Retrieve all four Account ID records assocaiated with a specific UPAI_18 
-- These four records have UPAI_15s of. 
select *
from RAW.salesforce_stitch.account
where id in(
    'INSERT LIST OF FOUR IDS ASSOCIATED'
)
