-- ETL Test 3b: Expand set of 204 to include all accounts associated with those UPAIs, which adds 35 account IDs
select ab.*,d.account_id as C_account_id,d.ultimate_parent_account_id as c_UPAI
from(
  select a.*,b.id as account_id,b.ultimate_parent_account_id
  from
    (
       select id,createddate,ULTIMATE_PARENT_ACCOUNT_ID__C
       from RAW.salesforce_stitch.account
       where isdeleted = FALSE and id is not null
    ) a
  inner join
  (
    select account_id as id,ultimate_parent_account_id
    from ANALYTICS.pwc_staging.sfdc_account
  ) b
    on a.id=b.id
  where createddate < date_From_parts(2019,8,21)
    and substr(b.ultimate_parent_account_id,1,15)<>ULTIMATE_PARENT_ACCOUNT_ID__C
) ab
right join
(
  select c.*
  from
  (
    select distinct ultimate_parent_account_id
    from
    (
      select id,createddate,ULTIMATE_PARENT_ACCOUNT_ID__C
      from RAW.salesforce_stitch.account
      where isdeleted = FALSE and id is not null
    ) a
    inner join
    (
      select account_id as id,ultimate_parent_account_id
      from ANALYTICS.pwc_staging.sfdc_account
    ) b
      on a.id=b.id
  where createddate < date_From_parts(2019,8,21)
  and substr(b.ultimate_parent_account_id,1,15)<>ULTIMATE_PARENT_ACCOUNT_ID__C
  ) ab
  inner join ANALYTICS.pwc_staging.sfdc_account c
    on ab.ultimate_parent_account_id=c.ultimate_parent_account_id
) d
  on ab.ultimate_parent_account_id=d.ultimate_parent_account_id
  and ab.account_id=d.account_id
