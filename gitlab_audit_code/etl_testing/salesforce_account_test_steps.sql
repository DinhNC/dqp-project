-- -- 
--  Prep list of all subscriptions that are in question
--  Test the Account that were `In Salesforce but missing from Snowflake`
--  Test the Account that were `In Snowflake but missing from Salesforce`
-- -- 

-- Set up the list of mis-matched subscriptions
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch AS
SELECT
'Not in Snowflake'::VARCHAR(100) AS type
,'insert_ultimate_parent_account_id_c'::VARCHAR(100) AS ultimate_parent_account_id_c
,'insert_account_id_18_c'::VARCHAR(100) AS account_id_18_c;

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch
VALUES
('Not in Snowflake','insert_ultimate_parent_account_id_c','insert_account_id_18_c'); 

-- Test if all rows in 
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch; 

SELECT type, COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch
GROUP BY 1;



-- --
-- Test the Account that were `In Salesforce but missing from Snowflake`
-- --
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_Salesforce_missing_snwflk;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_Salesforce_missing_snwflk AS
SELECT DISTINCT
    b.ultimate_parent_account_id_c, b.account_id_18_c, a.ULTIMATE_PARENT_ACCOUNT_ID__C, a.ACCOUNT_ID_18__C
FROM RAW.salesforce_stitch.account a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch b
    ON b.ultimate_parent_account_id_c = a.ULTIMATE_PARENT_ACCOUNT_ID__C
    AND b.account_id_18_c = a.ACCOUNT_ID_18__C
WHERE b.type = 'Not in Snowflake';

-- Test to see that there are 17 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_Salesforce_missing_snwflk;


-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_Salesforce_missing_snwflk
WHERE ultimate_parent_account_id_c IS NULL OR account_id_18_c IS NULL 
    OR ULTIMATE_PARENT_ACCOUNT_ID__C IS NULL OR ACCOUNT_ID_18__C IS NULL;




-- --
-- Test the Account that were `In Snowflake but missing from Salesforce`
-- --
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_snwflk_missing_Salesforce;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_snwflk_missing_Salesforce AS
SELECT DISTINCT
  b.ultimate_parent_account_id_c, b.account_id_18_c, a.ULTIMATE_PARENT_ACCOUNT_ID__C, a.ACCOUNT_ID_18__C
FROM RAW.salesforce_stitch.account a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch b
  ON b.ultimate_parent_account_id_c = a.ULTIMATE_PARENT_ACCOUNT_ID__C
  AND b.account_id_18_c = a.ACCOUNT_ID_18__C
WHERE b.type = 'Not in Salesforce';

-- Test to see that there are 3 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_snwflk_missing_Salesforce;


-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_snwflk_missing_Salesforce
WHERE ultimate_parent_account_id_c IS NULL OR account_id_18_c IS NULL 
    OR ULTIMATE_PARENT_ACCOUNT_ID__C IS NULL OR ACCOUNT_ID_18__C IS NULL;

-- --
-- Test to see if there are still ultimate parent acount mismatches - seeing results below match the salesforce api data pull. timing issue. 
-- --
SELECT ID, NAME, ULTIMATE_PARENT_ACCOUNT_ID__C,ACCOUNT_ID_18__C, SALES_SEGMENTATION_NEW__C
FROM Raw.salesforce_stitch.account
WHERE ID IN ('LIST_ID', 'LIST_ID'); -- only 2 rows with issues



-- -- 
-- Test to see if there are still acount mismatches 
-- -- 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_differnet_segments;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_differnet_segments AS
SELECT DISTINCT
  b.ultimate_parent_account_id_c, b.account_id_18_c, a.ULTIMATE_PARENT_ACCOUNT_ID__C, a.ACCOUNT_ID_18__C, A.NAME
FROM RAW.salesforce_stitch.account a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_mismatch b
  ON b.ultimate_parent_account_id_c = a.ULTIMATE_PARENT_ACCOUNT_ID__C
  AND b.account_id_18_c = a.ACCOUNT_ID_18__C
WHERE b.type ilike '%different%';

SELECT * FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.Salesforce_account_id_in_differnet_segments;


