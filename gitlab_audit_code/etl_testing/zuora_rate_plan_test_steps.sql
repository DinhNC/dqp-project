-- -- 
--  Prep list of all subscriptions that are in question
--  Test the Rate Plans that were `In Zuora but missing from Snowflake`
--  Test the Rate Plans that were `In Snowflake but missing from Zuora`
-- -- 

-- Set up the list of mis-matched subscriptions
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch AS
SELECT
'Not in Snowflake' AS type
,'INSERT RATE_PLAN_ID'::VARCHAR(100) AS rate_plan_id
,'INSERT RATE PLAN NAME'::VARCHAR(100) AS rate_plan_name;

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch
VALUES
('Not in Snowflake','INSERT RATE_PLAN_ID','INSERT RATE PLAN NAME'); 

-- Test if all rows in (should be 3399)
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch; -- Result: 3399

SELECT type, COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch
GROUP BY 1;
/*
Result:
TYPE	COUNT(*)
Not in Snowflake	3361
Not in Zuora	38
*/


-- -- 
-- Test the Rate Plans that were `In Zuora but missing from Snowflake`
-- -- 

DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_zuora_missing_snwflk;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_zuora_missing_snwflk AS
SELECT DISTINCT
    a.id, b.rate_plan_id, a.name, b.rate_plan_name
FROM RAW.zuora_stitch.rateplan a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch b
    ON a.id = b.rate_plan_id
WHERE b.type = 'Not in Snowflake';

-- Test to see that there are 3361 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_zuora_missing_snwflk;
-- Result: COUNT(*) = 3361

-- Test to see that there are 3361 rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_zuora_missing_snwflk
WHERE id IS NULL OR rate_plan_id IS NULL;
-- Result: COUNT(*) = 0 - all accounted for




-- -- 
-- Test the Rate Plans that were `In Snowflake but missing from Zuora`
-- -- 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_snwflk_missing_zuora;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_snwflk_missing_zuora AS
SELECT DISTINCT
    a.id, b.rate_plan_id, a.name, b.rate_plan_name
FROM RAW.zuora_stitch.rateplan a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_mismatch b
    ON a.id = b.rate_plan_id
WHERE b.type = 'Not in Zuora';

-- Test to see that there are 38 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_snwflk_missing_zuora;
-- Result: COUNT(*) = 38

-- Test to see that there are 3361 rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_id_in_snwflk_missing_zuora
WHERE id IS NULL OR rate_plan_id IS NULL;
-- Result: COUNT(*) = 0 - all accounted for

