-- --
--  Prep list of all subscriptions that are in question
--  Test the Invoice that were `In Zuora but missing from Snowflake`
--  Test the Invoice that were `In Snowflake but missing from Zuora`
-- test if that status is still different from zuora and snowflake
-- --

-- Set up the list of mis-matched subscriptions
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch AS
SELECT
'Not in Snowflake' AS type::VARCHAR(100)
,'INSERT_INVOICE_ID'::VARCHAR(100) AS invoice_id;

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch
VALUES
('Not in Snowflake','invoice_id');

-- Test if all rows in (should be 1429)
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch; -- Result: 1429

SELECT type, COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch
GROUP BY 1;
/*
Result:
TYPE	COUNT(*)
Not in Snowflake	1415
Not in Zuora	13
Different Status	1
*/


-- --
-- Test the Invoice that were `In Zuora but missing from Snowflake`
-- --

DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_zuora_missing_snwflk;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_zuora_missing_snwflk AS
SELECT DISTINCT
    a.id, b.invoice_id
FROM RAW.zuora_stitch.invoice a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch b
    ON a.id = b.invoice_id
WHERE b.type = 'Not in Snowflake';

-- Test to see that there are 1415 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_zuora_missing_snwflk;
-- Result: COUNT(*) = 1415

-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_zuora_missing_snwflk
WHERE id IS NULL OR invoice_id IS NULL;
-- Result: COUNT(*) = 0 - all accounted for




-- --
-- Test the Invoice that were `In Snowflake but missing from Zuora`
-- --
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_snwflk_missing_zuora;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_snwflk_missing_zuora AS
SELECT DISTINCT
    a.id, b.invoice_id
FROM RAW.zuora_stitch.invoice a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch b
    ON a.id = b.invoice_id
WHERE b.type = 'Not in Zuora';

-- Test to see that there are 13 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_snwflk_missing_zuora;
-- Result: COUNT(*) = 13

-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_in_snwflk_missing_zuora
WHERE id IS NULL OR invoice_id IS NULL;
-- Result: COUNT(*) = 0 - all accounted for

-- --
-- test if that status is still different from zuora and snowflake
-- --

SELECT DISTINCT
    a.id, a.status, b.invoice_id
FROM RAW.zuora_stitch.invoice a
INNER JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_invoice_id_mismatch b
    ON a.id = b.invoice_id
    AND b.type  = 'Different Status';


-- Then compare result to the Zuora Reporting > Data Sources > Invoice table 
