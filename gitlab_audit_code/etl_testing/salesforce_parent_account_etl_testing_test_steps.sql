--------------
-- 1. Ultimate Parent ID 15 character length mismatch
--------------
-- First re-run the auditor's scripts against the copy of the table they used. 
SELECT 
  case when a.id is null then 1 
    else 0 
  end as MSNG_A,
  case when b.id is null then 1 
    else 0 
  end as MSNG_B,
  case when createddate > date_From_parts(2019,8,21) then 1 
    else 0 
  end as CREATED_LATER,
  case when substr(b.ultimate_parent_account_id,1,15)=ULTIMATE_PARENT_ACCOUNT_ID__C then 1  -- the substring function is missing regex edits
    else 0 
  end as MATCH_UPAI_15,
  count(*)
from
(
  select id,createddate,ULTIMATE_PARENT_ACCOUNT_ID__C
  from RAW.salesforce_stitch.account
  where isdeleted = FALSE and id is not null
) a
left join
(
  select account_id as id,ultimate_parent_account_id -
  from ANALYTICS.pwc_staging.sfdc_account
) b
  on a.id=b.id
group by 1,2,3,4

-- Next run the script against the current sfdc_account table in the ANALYTICS database. 
SELECT 
  case when a.id is null then 1 
    else 0 
  end as MSNG_A,
  case when b.id is null then 1 
    else 0 
  end as MSNG_B,
  case when createddate > date_From_parts(2019,8,21) then 1 
    else 0 
  end as CREATED_LATER,
  case when substr(b.ultimate_parent_account_id,1,15)=ULTIMATE_PARENT_ACCOUNT_ID__C then 1 
    else 0 
  end as MATCH_UPAI_15,
  count(*)
from
(
  select id,createddate,ULTIMATE_PARENT_ACCOUNT_ID__C
  from RAW.salesforce_stitch.account
  where isdeleted = FALSE and id is not null
) a
left join
(
  select account_id as id,ultimate_parent_account_id
  from ANALYTICS.ANALYTICS_STAGING.sfdc_account
) b
  on a.id=b.id
group by 1,2,3,4

-- Next simplify the script. The idea of the script is to compare the data present in the RAW.salesforce_stitch.account against ANALYTICS.ANALYTICS_STAGING.sfdc_account looking at the id, createddate, and ULTIMATE_PARENT_ACCOUNT_ID__C columns. 

-- Set up the list of mis-matched subscriptions
DROP TABLE IF EXISTS ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches;
CREATE OR REPLACE TABLE ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches AS
SELECT DISTINCT 
  raw.id, 
  raw.createddate, 
  raw.ultimate_parent_account_id__c, 
  trim(substring(regexp_replace(raw.ultimate_parent_account_id__c,'_HL_ENCODED_/|<a\\s+href="/', ''), 0, 15)) AS regex_version,
  staging.account_id, 
  staging.utimate_parent_id, 
  staging.ultimate_parent_account_id, 
  staging.created_date, 
  raw.isdeleted AS raw_is_deleted,
  staging.is_deleted AS staging_is_deleted,
  IFF(raw.id IS NOT NULL, 0, 1) AS is_missing_from_raw,
  IFF(staging.account_id IS NOT NULL, 0, 1) AS is_missing_from_staging,
  IFF(raw.ultimate_parent_account_id__c = staging.utimate_parent_id, 0, 1) AS is_ultimate_parent_account_id_mismatch,
  IFF(regex_version = trim(substr(staging.ultimate_parent_account_id, 0, 15)), 0, 1) AS is_ultimate_parent_account_id_regex_mismatch,
  IFF(raw.createddate = staging.created_date, 0, 1) AS is_created_date_mismatch
FROM RAW.salesforce_stitch.account raw
LEFT JOIN ANALYTICS.analytics_staging.sfdc_account staging
    ON raw.id = staging.account_id
    AND raw.id IS NOT NULL
WHERE raw.isdeleted = FALSE;    
    
SELECT 
  'missing_from_raw' AS metric,
  SUM(is_missing_from_raw) AS total
FROM ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches
UNION
SELECT 
  'missing_from_staging' AS metric,
  SUM(is_missing_from_staging) AS total
FROM ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches
UNION
SELECT 
  'ultimate_parent_account_id_mismatch' AS metric,
  SUM(is_ultimate_parent_account_id_mismatch) AS total
FROM ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches
WHERE is_missing_from_staging = 0
UNION
SELECT 
  'ultimate_parent_account_id_regex_mismatch' AS metric,
  SUM(is_ultimate_parent_account_id_regex_mismatch) AS total
FROM ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches
WHERE is_missing_from_staging = 0
UNION
SELECT 
  'created_date_mismatch' AS metric,
  SUM(is_created_date_mismatch) AS total
FROM ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches
WHERE is_missing_from_staging = 0

-- missing_from_staging = 12
-- missing_from_raw = 0
-- ultimate_parent_account_id_regex_mismatch = 0
-- ultimate_parent_account_id_mismatch = 0
-- created_date_mismatch = 0

SELECT DISTINCT createddate::DATE
FROM ANALYTICS.datwood_scratch_analytics.salesforce_account_parent_account_mismatches
WHERE is_missing_from_staging = 1

--2020-01-02, date query was run


 
