-------------------
-- Prep List of all subscriptions that are in question
-- Set up the list of subscriptions not in Snowflake but not in Zuora - TESTING 
-- Set up the list of subscriptions in Zuora but not in Snowflake - CANNOT CONFIRM WITH SNOWFLAKE (need check in zuora)
-- Set up the list of different statuses - TESTING 
-- Set up the list of different effective contract dates - CANNOT CONFIRM WITH SNOWFLAKE (there was zero results)
-------------------

-- Set up the list of subscriptions not in Snowflake but not in Zuora 

DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_missing_in_snowflake_information;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_missing_in_snowflake_information AS
SELECT 'INSERT SUBSCRIPTION ID' AS subscription_id,
  'INSERT SUBSCRIPTION NAME' AS subscription_name;


INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_missing_in_snowflake_information
VALUES
('INSERT SUBSCRIPTION ID','INSERT SUBSCRIPTION NAME');



-- Set up the list of different statuses
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_with_different_status;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_with_different_status AS
SELECT 
  'INSERT HERE'                      AS subscription_id,
  'INSERT HERE'::VARCHAR             AS subscription_name
  'INSERT HERE'                      AS subscription_status
  ;


INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_with_different_status
VALUES
('INSERT HERE','INSERT HERE','INSERT HERE'),



-------------------
-- Test to see if the subscriptions that were previously not found in Snowflake are in the warehouse today 
-------------------
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscription_NOTINSNF;  
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscription_NOTINSNF AS  
SELECT DISTINCT 
    a.id, a.name, b.subscription_id, b.subscription_name 
FROM RAW.zuora_stitch.subscription a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_missing_in_snowflake_information b 
    ON a.id = b.subscription_id
    AND a.name = b.subscription_name; 
    
SELECT * 
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscription_NOTINSNF
WHERE id IS NULL
  OR name IS NULL; 
-- Query returns 21 rows where we are still missing information in the raw table that is in zuora 


-------------------
-- Test to see if the subscriptions that were previously not found in Zuora but were in Snowflake 
-------------------

/*
This test needs to be conducted in Zuora UI. One would need to check if those are in Zuora, and if not, we need to reach
out and understand if we should delete this from Snowflake or why we have it in Snowflake but it is no longer in Zuora. 

Therefore, we will not be including code here. 
*/

-------------------
-- Test to see if the subscriptions that had a different status between the Zuora and Snowflake environment 
-------------------

-- Looking at the data (837 rows) presented by the auditors, there were two cases: 
-- 1) When the Zuora UI said the subscription was `expired` but the Snowflake environment said it was `active` (836 rows).
-- 2) When the Zuora UI said the subscription was `active` but Snowflake environment said it was `expired` (1 row). 

DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscriptions_different_status;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscriptions_different_status AS
SELECT DISTINCT
    a.id, a.name, a.status, b.subscription_id, b.subscription_name, b.subscription_status
FROM RAW.zuora_stitch.subscription a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_subscriptions_with_different_status b
    ON a.id = b.subscription_id
    AND a.name = b.subscription_name;

SELECT DISTINCT status, subscription_status
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscriptions_different_status; 
/*
RESULTS: 
STATUS	SUBSCRIPTION_STATUS
Expired	Expired
NULL  	Expired
*/

SELECT *
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.subscriptions_different_status
WHERE status is NULL;
-- There are three results being returned where the data is not in our raw schema in our data warehouse but is in zuora. 



-------------------
-- Test to see if the subscriptions that had a different contract effective date between the Zuora and Snowflake environment 
-------------------

/*
Given that that auditors also did not find any, we are not able to test their SQL on the previous data sets, and we cannot check this further. 

Therefore, we will not be including code here. 
*/
