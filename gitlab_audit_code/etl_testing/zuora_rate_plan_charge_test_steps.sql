-- -- 
--  Prep list of all subscriptions that are in question
--  Test the Rate Plan Charge that were `In Zuora but missing from Snowflake`
--  Test the Rate Plan Charge that were `In Snowflake but missing from Zuora`
-- -- 

-- Set up the list of mis-matched subscriptions
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch AS
SELECT
'Not in Snowflake' AS type
,'INSERT rate_plan_charge_ID'::VARCHAR(100) AS rate_plan_charge_id;

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch
VALUES
('Not in Snowflake','INSERT rate_plan_charge_ID'), 
('Not in Zuora','INSERT rate_plan_charge_ID'); 

-- Test if all rows in (should be 4137 + 57)
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch; 

SELECT type, COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch
GROUP BY 1;


-- -- 
-- Test the Rate Plan Charge that were `In Zuora but missing from Snowflake`
-- -- 

DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_zuora_missing_snwflk;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_zuora_missing_snwflk AS
SELECT DISTINCT
    a.id, b.rate_plan_charge_id
FROM RAW.zuora_stitch.rateplancharge a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch b
    ON a.id = b.rate_plan_charge_id
WHERE b.type ilike '%Snowflake%'; -- for some reason when reading into snowflake, it creaeted two distinct types of 'Not in Snowflake' values ... 

-- Test to see that there are 4137 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_zuora_missing_snwflk;

-- Test to see that there are any rows returned 
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_zuora_missing_snwflk
WHERE id IS NULL OR rate_plan_charge_id IS NULL;





-- -- 
-- Test the Rate Plan Charge that were `In Snowflake but missing from Zuora`
-- -- 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_snwflk_missing_zuora;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_snwflk_missing_zuora AS
SELECT DISTINCT
    a.id, b.rate_plan_charge_id
FROM RAW.zuora_stitch.rateplancharge a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_mismatch b
    ON a.id = b.rate_plan_charge_id
WHERE b.type = 'Not in Zuora';

-- Test to see that there are 57 rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_snwflk_missing_zuora;

-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_rate_plan_charge_id_in_snwflk_missing_zuora
WHERE id IS NULL OR rate_plan_charge_id IS NULL;


