-------------------
-- Prep List of all CRMs that are in question 
-------------------

-- Create a table with all the CRMs that are missing from Snowflake 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_in_zuora_not_snowflake; 
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_in_zuora_not_snowflake AS 
SELECT 'INSERT CRM ID' AS zuora_crm_id ; 

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_in_zuora_not_snowflake
VALUES ('INSERT CRM ID'), ('INSERT CRM ID');

SELECT zuora_crm_id FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_in_zuora_not_snowflake;

-- Create a table with all the CRMs that are missing from Zuora  
-- This information needs to be checked on Zuora UI. 


-- Create a table with all the CRMs account_ids that have different crms: 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs; 
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs AS 
SELECT  'INSERT CRM ID' AS zuora_crm_id, 'INSERT ZUORA ACCOUNT ID' AS zuora_account_id; 

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs
VALUES ('INSERT CRM ID', 'INSERT CRM ID');

SELECT zuora_crm_id, zuora_account_id FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs;

-------------------
-- Search for the crms that were previously missing from snowflake 
-- https://gitlab.com/gitlab-com/business-ops/dqp-data-quality-process/dqp-project/issues/7#note_245755394
-------------------
-- The below query informs us if that crm is now present in Snowflake in the RAW schema 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.account_NOTINSNF;  
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.account_NOTINSNF AS  
SELECT DISTINCT 
    a.id, 
    a.createddate, 
    a.crmid, 
    a.name, 
    a.deleted, 
    b.zuora_crm_id
FROM RAW.zuora_stitch.account a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_in_zuora_not_snowflake b 
    ON a.crmid = b.zuora_crm_id; 
    
SELECT * 
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.account_NOTINSNF
WHERE crmid IS NULL;  -- This returns zero rows. This is most likely due to a timing issue. 


-- The below query informs us if that crm is now present in Snowflake in the Staging schema 
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.account_NOTINSNF_staging;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.account_NOTINSNF_staging AS  
SELECT DISTINCT account_id, created_date, crm_id, account_name, b.zuora_crm_id
FROM ANALYTICS.analytics_staging.zuora_account a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_in_zuora_not_snowflake b 
    ON a.crm_id = b.zuora_crm_id; 
    
SELECT * 
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.account_NOTINSNF_staging
WHERE crmid IS NULL;  -- This returns zero rows. This is most likely due to a timing issue. 


-------------------
-- Search for the crms that were previously missing from zuora but in snowflake  
-- https://gitlab.com/gitlab-com/business-ops/dqp-data-quality-process/dqp-project/issues/7#note_245762447
-------------------
-- This needs to be checked in Zuora and with the business owner of Zuora. 


-------------------
-- Search for the crms that are both in snowflake and in zuora environment data 
-- https://gitlab.com/gitlab-com/business-ops/dqp-data-quality-process/dqp-project/issues/7#note_245763329
-------------------
-- The below query informs us if that crm is now present in Snowflake in the RAW schema
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs_in_raw_schema;  
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs_in_raw_schema AS  
SELECT DISTINCT
    a.zuora_account_id,
    a.createddate,
    a.crmid,
    a.name,
    a.deleted,
    b.zuora_crm_id
FROM RAW.zuora_stitch.account a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs b
    ON a.crmid = b.zuora_crm_id
    AND a.id = b.zuora_account_id;

SELECT * 
FROM  ANALYTICS.KTAM_SCRATCH_ANALYTICS.crm_account_crm_id_diffs_in_raw_schema
WHERE zuora_account_id IS NULL OR zuora_crm_id IS NULL;




