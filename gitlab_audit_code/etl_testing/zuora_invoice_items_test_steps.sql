-- --
--  Prep list of all subscriptions that are in question
--  Test the Invoice_Items that were `In Zuora but missing from Snowflake`
--  Test the Invoice_Items that were `In Snowflake but missing from Zuora`
-- --

-- Set up the list of mis-matched subscriptions
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch AS
SELECT
'Not in Snowflake'::VARCHAR(100) AS type
,'INSERT_Invoice_Items_ID'::VARCHAR(100) AS Invoice_Items_id;

INSERT INTO ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch
VALUES
('Not in Snowflake','Invoice_Items_id');

-- Test if all rows in (should be #)
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch; 

SELECT type, COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch
GROUP BY 1;



-- --
-- Test the Invoice_Items that were `In Zuora but missing from Snowflake`
-- --

DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_zuora_missing_snwflk;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_zuora_missing_snwflk AS
SELECT DISTINCT
    a.id, b.Invoice_Items_id
FROM RAW.zuora_stitch.invoiceitem a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch b
    ON a.id = b.Invoice_Items_id
WHERE b.type = 'Not in Snowflake';

-- Test to see that there are  rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_zuora_missing_snwflk;


-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_zuora_missing_snwflk
WHERE id IS NULL OR Invoice_Items_id IS NULL;


-- --
-- Test the Invoice_Items that were `In Snowflake but missing from Zuora`
-- --
DROP TABLE IF EXISTS ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_snwflk_missing_zuora;
CREATE OR REPLACE TABLE ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_snwflk_missing_zuora AS
SELECT DISTINCT
    a.id, b.Invoice_Items_id
FROM RAW.zuora_stitch.invoiceitem a
RIGHT JOIN ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_mismatch b
    ON a.id = b.Invoice_Items_id
WHERE b.type = 'Not in Zuora';

-- Test to see that there are  rows
SELECT COUNT(*) FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_snwflk_missing_zuora;


-- Test to see that there are any rows
SELECT COUNT(*)
FROM ANALYTICS.KTAM_SCRATCH_ANALYTICS.zuora_Invoice_Items_id_in_snwflk_missing_zuora
WHERE id IS NULL OR Invoice_Items_id IS NULL;

