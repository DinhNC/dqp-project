<!---
This issue is for a metric change according to the Data Quality Process in the handbook. https://about.gitlab.com/handbook/business-ops/
---->

#### Which metric is changing?


#### What are the relevant code issues/MRs?


#### Why is the change happening?


#### Is this a drastic enough change to require immediate board notification?


#### Specific Tasks

- [ ] Code and Process Review
  - [ ] Upstream system changes documented
  - [ ] Extraction validated, documented, and tested
  - [ ] Transformations validated, documented, and tested
  - [ ] Looker Models validated and documented
- [ ] Before & After screenshots of metric values
- [ ] Change summary documented in a CHANGELOG


#### Notifications

- [ ] Dir. of BusOps @wzabaglio
- [ ] Additional, relevant stakeholders
- [ ] CFO @pmachle
- [ ] CEO @sytses
- [ ] Board

This can be closed once the update has been added to the next board deck.

/label ~"DPQ"